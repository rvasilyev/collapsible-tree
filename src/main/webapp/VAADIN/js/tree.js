(function () {


    function create(element, itemProvider) {
        var root,
            svg = d3.select(element).append("svg")
                .attr("width", "100%")
                .attr("height", "100%"),
            linkDistance,
            size,
            force = d3.layout.force()
                .linkStrength(0.2)
                .friction(0.7)
                .on("tick", tick),
            linkSelector = svg.selectAll(".link"),
            nodeSelector = svg.selectAll(".node"),
            lastExpandedDepartment,
            infoBox = d3.select(element).append("div").classed("infoBox", true),
            minSize;
        doSize();

        function doSize() {
            size = [svg[0][0].clientWidth, svg[0][0].clientHeight];
            minSize = Math.min.apply(Math, size);
            linkDistance = minSize / 15;
            force
                .size(size)
                .gravity(300 / minSize)
                .charge( - minSize / 3)
                .linkDistance(function (link) {
                    if (link.source.type == "organization") {
                        return linkDistance * 5;
                    } else {
                        return linkDistance;
                    }
                });
        }

        function resize() {
            doSize();
            update();
        }

        window.addEventListener("resize", resize);

        function tick() {
            linkSelector.attr("x1", function (link) {
                return link.source.x;
            }).attr("y1", function (link) {
                return link.source.y;
            }).attr("x2", function (link) {
                return link.target.x;
            }).attr("y2", function (link) {
                return link.target.y;
            });

            nodeSelector.attr("transform", function (node) {
                return "translate(" + node.x + "," + node.y + ")";
            });
        }


        function children(d) {
            return d.children || [];
        }

        function getChildren(d, callback) {
            itemProvider(d.type, d, callback);
        }

        function toggle(d) {
            if (d.children) {
                d.children = null;

            } else {
                if (d.type == "employee") {
                    return;
                }
                if (lastExpandedDepartment) {
                    lastExpandedDepartment.children = null;
                    lastExpandedDepartment = null;
                }
                d.loading = true;
                getChildren(d, function (children) {
                    d.loading = false;
                    d.children = children;
                    children.forEach(function (child) {
                        child.parent = d;
                    });
                    var len = children.length;
                    children.forEach(function (c, i) {
                        c.x = d.x + linkDistance * 0.7 * Math.cos(2 * Math.PI * i / len);
                        c.y = d.y + linkDistance * 0.7 * Math.sin(2 * Math.PI * i / len);
                    });
                    if (d.type == "department") {
                        lastExpandedDepartment = d;
                    }

                    update();
                })
            }
            update();
        }


        function update() {
            if (!root) {
                return;
            }
            var nodes = [].concat.apply([root].concat(children(root)), children(root).map(children));
            var links = d3.layout.tree().links(nodes);
            root.x || (root.x = size[0] / 2);
            root.y || (root.y = size[1] / 2);
            force.nodes(nodes).links(links).start();


            nodeSelector = nodeSelector.data(nodes);
            nodeSelector.classed("loading", function (d) {
                return d.loading;
            });


            var groups = nodeSelector.enter()
                .append("g")
                .attr("class", "node")
                .on("click.toggle", toggle)
                .on("mouseover.hover", function (d) {
                    if (d.type != "employee") {
                        return;
                    }
                    infoBox.html(["<p>name: ", d.name, "<p>", d.parent && d.parent.name, "<p>Tel: ", d.tel].join("")).classed("infoBox-visible", true);
                })
                .on("mouseout.hover", function () {
                    infoBox.html("").classed("infoBox-visible", false)
                });

            groups
                .append("circle")
                .attr("r", 7);

            groups.append("text").attr("dy", "20").text(function (d) {
                if (d.type == "employee") {
                    return "";
                }
                return d.name
            });

            nodeSelector.exit().remove();


            linkSelector = linkSelector.data(links);
            linkSelector
                .enter()
                .insert("line", ".node")
                .attr("class", "link");
            linkSelector.exit().remove();
        }

        function setRoot(rootNode) {
            root = rootNode;
            update();
        }

        return {
            setRoot: setRoot
        }
    }

    window.TreeComponent = create;
})();
