function ru_rvasilyev_demo_TreeComponent() {
    var component = this, currentCallback, expandingNode;

    function adapter(type, node, callback) {
        component.query(type, {
            id: node.id,
            type: node.type
        });
        currentCallback = callback;
        expandingNode = node;
    }

    component.callback = function (id, data) {
        if (expandingNode.id == id && currentCallback) {
            currentCallback(JSON.parse(data));
        }
    };
    var tree = TreeComponent(this.getElement(), adapter);
    component.setRoot = function (org) {
        org = JSON.parse(org);
        tree.setRoot({
            id: org.orgId,
            name: org.orgName,
            type: "organization"
        });
    }

}
