package ru.rvasilyev.demo;

import com.google.gson.Gson;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.JavaScriptFunction;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import ru.rvasilyev.demo.service.OrgService;

import java.util.List;

/**
 * @author renat
 * @version 05.09.2015
 */
@JavaScript({"vaadin://js/d3.js", "vaadin://js/tree.js", "vaadin://js/tree-vaadin.js"})
@StyleSheet("vaadin://tree.css")
public class TreeComponent extends AbstractJavaScriptComponent {
    private final static Gson gson = new Gson();
    final OrgService service;

    public TreeComponent(final OrgService service) {
        this.service = service;
        addStyleName("tree-container");
        addFunction("query", new JavaScriptFunction() {
            @Override
            public void call(JsonArray args) {
                String type = args.getString(0);
                JsonObject object = args.getObject(1);
                String id = object.getString("id");
                switch (type) {
                    case "organization":
                        List<Department> departments = service.queryDepartments(id);
                        callFunction("callback", id, gson.toJson(departments));
                        break;
                    case "department":
                        List<Employee> employees = service.queryEmployees(id);
                        callFunction("callback", id, gson.toJson(employees));
                }
            }
        });
    }

    public void setRoot(Organization organization){
        callFunction("setRoot", gson.toJson(organization));
    }

}
