package ru.rvasilyev.demo;

/**
 * @author renat
 * @version 05.09.2015
 */
public class Employee {
    final String name;
    final String tel;
    final String type = "employee";

    public Employee(String name, String tel) {
        this.name = name;
        this.tel = tel;
    }
}
