package ru.rvasilyev.demo.service;

import ru.rvasilyev.demo.Department;
import ru.rvasilyev.demo.Employee;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author renat
 * @version 05.09.2015
 */
public class OrgService {
    static ThreadLocalRandom random = ThreadLocalRandom.current();
    List<String> names = Arrays.asList("Dion Settles",
            "Daniele Pennebaker",
            "Lucio Smartt",
            "Irene Guthrie",
            "Domonique Loucks",
            "Karine Merrell",
            "Krysta Brotherton",
            "Debbra Klem",
            "Russel Paxson",
            "Tasha Lareau",
            "Holley Bashore",
            "Tyisha Enright",
            "Dawna Slick",
            "Marlo Muirhead",
            "Jeneva Fern",
            "Harriet Dycus",
            "Cherry Corriveau",
            "Era Ries",
            "Garfield Bonavita",
            "Glennis Webster",
            "Mercedez Finley",
            "Roma Guertin",
            "Vikki Defazio",
            "Delorse Lindsley",
            "Zachariah Stonge",
            "Genesis Park",
            "Dorothy Kolodziej",
            "Yee Neubert",
            "Easter Kumar",
            "Mallie Madrigal",
            "Audrey Rausch",
            "Luvenia Orris",
            "Titus Clepper",
            "Willa Dejohn",
            "Teresia Capps",
            "Ignacia Locust",
            "Gertie Magby",
            "Madeline Wakeland",
            "Nicola Mcfarlain",
            "Arvilla Spangler",
            "Steven Gauer",
            "Dulcie Kuster",
            "Sherise Casazza",
            "Delphia Rowlands",
            "Orpha Enriquez",
            "Madison Vince",
            "Stella Tavera",
            "Roberta Rogan",
            "Kathy Mcnickle",
            "Eric Krasner",
            "Jenee Benzing",
            "Mardell Bogard", "Marlen Prejean", "Ashley Froman", "Larry Rentschler", "Moon Freed", "Alexia Mahn", "Kerrie Egnor", "Eura Mobley", "Sherrie Netto", "Mireya Revelle", "Alonzo Thatch", "Antwan Tranmer", "Lupe Harrold", "Casimira Gorby", "Magaly Ligon", "Kristle Magno", "Consuela Counce", "Miquel Cornforth", "Reynalda Ostby", "Rolando Gamon", "Lillia Grosch", "Alessandra Mcglamery", "Ailene Shasteen", "Celina Barnum", "Obdulia Croney", "Malika Aikens", "Felton Infante", "Charla Landi", "Virgilio Knupp", "Pasty Greenhouse", "Erna Kicklighter", "Wilma Delgadillo", "Cinda Pizzuto", "Idell Earley", "Nakisha Cloninger", "Shanon Tauber", "Bernetta Harshbarger", "Chet Fan", "Candice Stiverson", "Cordia Edmonson", "Penney Hesler", "Zenobia Maes", "Bernie Hyman", "Dede Naumann", "Bernarda Blackwelder", "Scottie Philbrook", "Alesia Ronning", "Julius Kincade"
    );

    public List<Department> queryDepartments(String id) {
        return Arrays.asList(
                new Department("0", "IT"),
                new Department("1", "Sales"),
                new Department("2", "QA"),
                new Department("3", "R&D")
        );
    }

    public List<Employee> queryEmployees(String id) {
        return names.stream().map(OrgService::randomEmployee).collect(Collectors.toList());
    }

    static  Employee randomEmployee(String name) {
        return new Employee(name, "+"+random.nextLong(1000_000_00_00l));
    }

    public String queryOrgName(String id) {
        return "Acme Inc.";
    }
}
