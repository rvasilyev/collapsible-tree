package ru.rvasilyev.demo;

/**
 * @author renat
 * @version 05.09.2015
 */
public class Organization  {
    public String orgId;
    public String orgName;

    public Organization(String orgId, String orgName) {
        this.orgId = orgId;
        this.orgName = orgName;
    }
}
