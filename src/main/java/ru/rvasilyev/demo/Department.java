package ru.rvasilyev.demo;

/**
 * @author renat
 * @version 05.09.2015
 */
public class Department {
    final String id;
    final String name;

    final String type = "department";

    public Department(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
